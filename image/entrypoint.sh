#!/usr/bin/env bash

# If invoked with env var NODE_NAME **unset**, this script will:
#   1. find all k8s master nodes in need of etcd defragmentation
#   2. spawn a k8s job/etcd-defrag-${hostname}-${date} for each such node using
#      the same image + entrypoint; the job is instantiated from
#      cronjob/etcd-defrag-host residing in the same namespace.
# Otherwise, it will defragment etcd instance listening on
# https://127.0.0.1:2379 as long as it is healthy.

set -euo pipefail

# default variables
: "${DB_USE_RATIO_THRESHOLD:=0.55}"
: "${CERT_DIR:=/}"
: "${PROMETHEUS_URL:=http://prometheus-operated.monitoring.svc:9090}"
: "${CTRL_NODE_SELECTOR:=node.k0sproject.io/role=control-plane}"

DB_USE_RATIO_QUERY='(last_over_time(etcd_mvcc_db_total_size_in_use_in_bytes[5m])
/ last_over_time(etcd_mvcc_db_total_size_in_bytes[5m]))'
SAFETY_QUERY='rate(etcd_server_leader_changes_seen_total[2m]) > 0
or absent(etcd_server_leader_changes_seen_total) == 1'

function _etcdctl() {
    etcdctl \
        --cert="${CERT_DIR}/client.crt" \
        --key="${CERT_DIR}/client.key" \
        --cacert="${CERT_DIR}/ca.crt" "$@"
}

function promquery() {
    curl --get --silent "$PROMETHEUS_URL/api/v1/query" --data-urlencode query="$1"
}

# returns the list of schedulable ctrl plane hostnames in need of etcd defragmentation
# where the current etcd leader is listed last
function get_hosts() {
    local is_leader highly_fragmented
    is_leader="$(promquery etcd_server_is_leader | jq -c .)"
    # returns only instances in need for fragmentation (most of the times it should be empty)
    highly_fragmented="$(promquery "$DB_USE_RATIO_QUERY < $DB_USE_RATIO_THRESHOLD" | jq -c .)"
    if jq --exit-status --slurp '[.[] | (.status // "") != "success"] | any' >/dev/null \
            < <(printf '%s\n' "$is_leader" "$highly_fragmented");
    then
        printf 'Failed to query prometheus!\n' >&2
        return 1
    fi
    jq -r --argjson highly_fragmented "${highly_fragmented}" --argjson is_leader "${is_leader}" \
          --argjson threshold "${DB_USE_RATIO_THRESHOLD}" '.
        # filter out unschedulable nodes
        | [.items[] | { "key": .metadata.labels["kubernetes.io/hostname"]
                      , "value": ((.spec.unschedulable // false) | not)}
          ] | from_entries as $schedulable
        # sort schedulable nodes from the most higly fragmented to the least
        | $highly_fragmented | .data.result | sort_by(.value[1] | tonumber)
                             | [ .[] | .metric.instance
                               | select($schedulable[.])
                               ] as $candidates
        # determine leaders -- host_is_leader maps hostname to is_leader (bool)
        | $is_leader | [.data.result[] | {"key": .metric.instance, "value": ((.value[1] | tonumber) == 1)}]
                     | from_entries | . as $host_is_leader | $host_is_leader
        # shift the leader to the end of the list to reduce the number of re-elections
        | $candidates | sort_by($host_is_leader[.]) as $sorted  # we rely on a stable sort (since jq 1.6)
        # print candidates and reasons to stderr
        | [ .[] | . as $c
          | [ "Found a candidate for etcd defragmentation: \($c): is_leader=\($host_is_leader[.]);"
            , "dbSizeInUse / dbSizeTotal = \([ $highly_fragmented.data.result[]
                                             | select(.metric.instance == $c) | .value[1] | tonumber][0])"
            , "<", "\($threshold)"
            ] | join(" ")
          | debug
          ]
        | $sorted[]' < <(kubectl get nodes -l "${CTRL_NODE_SELECTOR}" -o json)
}

# create a k8s job from cronjob/etcd-defrat-host template and wait for its termination
function run_defrag_host_job() {
    local hostname="$1"
    local escaped job_name watch_pid
    escaped="$(tr -c 'a-z0-9-' '-' <<<"${hostname%%.*}" | sed 's/-\+$//')"
    job_name="etcd-defrag-${escaped}-$(date '+%Y%m%d-%H%M')"
    kubectl create job --dry-run=client "${job_name}" --from=cronjob/etcd-defrag-host --output=json | \
        jq --arg hostname "$hostname" '.spec.template.spec.nodeSelector["kubernetes.io/hostname"] |= $hostname
            | .metadata.labels.target_host |= $hostname' | \
        kubectl create -f -
    printf 'Creating a job/%s ...\n' >&2 "$job_name"

    watch_pid="$(mktemp)"
    ( \
        timeout 2m kubectl get --watch \
            -o jsonpath='{.status.failed}:{.status.succeeded}{"\n"}' job/"$job_name" & \
        echo "$!" >"$watch_pid"; \
    ) | \
        while IFS=: read -r failed succeeded;
    do
        if [[ -z "${failed:-}${succeeded:-}" ]]; then
            continue
        fi
        local ret=0
        if [[ "${failed:-0}" -gt 0 ]]; then
            printf 'Job/%s failed! Terminating ...\n' >&2 "$job_name"
            ret=1
        fi
        kill -TERM "$(<"$watch_pid")" ||:
        wait "$(<"$watch_pid")" 2>/dev/null ||:
        rm "$watch_pid"
        return "$ret"
    done
}

# makes sure etcd instances are up there has been no re-election during the past X minutes
function safety_check() {
    promquery "$SAFETY_QUERY" | jq --exit-status '.status == "success" and .data.result == []' >/dev/null
}

function spawn_jobs() {
    if ! safety_check; then
        printf 'Health check failed, skipping defragmentation.\n' >&2
        return 1
    fi
    local host hosts hostsarr=()
    hosts="$(get_hosts)"
    if [[ -z "${hosts:-}" ]]; then
        printf 'No need for defragmentation.\n' >&2
        return 0
    fi
    readarray -t hostsarr <<<"$hosts"
    for host in "${hostsarr[@]}"; do
        if ! run_defrag_host_job "$host"; then
            return 1
        fi
    done
}

function defragment_etcd() {
    if [[ "$(printf '(0 < %f) && (%f < 1)\n' \
            "$DB_USE_RATIO_THRESHOLD" "$DB_USE_RATIO_THRESHOLD" | bc)" != 1 ]];
    then
        printf 'DB_USE_RATIO_THRESHOLD=%s must be a number between 0 and 1.\n' >&2 \
            "$DB_USE_RATIO_THRESHOLD"
        exit 1
    fi

    if ! _etcdctl endpoint health -w json | jq --exit-status '[.[] | .health] | all and (length > 0)' >/dev/null; then
        printf 'etcd is not healthy! Skipping defragmentation.\n' >&2
        return 1
    fi
    _etcdctl defrag
}

function main() {
    if [[ -n "${NODE_NAME:-}" ]]; then
        defragment_etcd
    else
        # spawned jobs will run the same entrypoint and will have NODE_NAME set
        spawn_jobs
    fi
}

# are we executed or sourced?
if ! ( return 0 >/dev/null 2>&1; ); then
    # => executed
    main
fi
