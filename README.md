# Automated etcd defragmentation

## Motivation

Etcd is being compacted periodically. On our current k0s it happens every 5
minutes by default. The side effect is etcd database becoming fragmented and
occupying more storage than it actually needs.

We receive several [etcdDatabaseHighFragmentationRatio
alerts](https://github.com/prometheus-community/helm-charts/blob/33697be/charts/kube-prometheus-stack/templates/prometheus/rules-1.14/etcd.yaml#L443)
each month which requires manual intervetion (toil). The alert is triggered
when etcd database's $`\frac{\text{sizeInUse}}{\text{sizeTotal}} < 0.5`$.

This is an automated solution to the problem leveraging [etcd's defragmentation
mechanism](https://etcd.io/docs/v3.2/op-guide/maintenance/#defragmentation).

## Caveats

- It is a potentially dangerous operator, which should not be performed on
  unhealthy cluster.
- Defragmentation operation applies to a single etcd instance.
- While being defragmented, the etcd instance is locked both for writing and reading.
- On k0s, etcd listens for client connections only on `https://127.0.0.1:2379`
  on the **host** network. They cannot be reached from pod network.

## Dependencies

- prometheus instance scraping etcd
- kubectl and etcdctl binaries matching the versions of their servers available
  on the hosts

## How it works

CronJob/etcd-defrag is run periodically according to the defined schedule. It
does the following:

1. checks for safety: all instances are up and there has been no leader
   election recently
1. determines schedulable control plane nodes in need of etcd fragmentation
   - where "in need of etcd defragmentation" means slightly before an alert would
     have been triggered (e.g. when
     $`\frac{\text{sizeInUse}}{\text{sizeTotal}} < 0.55`$)
1. schedules a job for each such node sequentially that defragments the
   instances so that:
   - the current etcd leader is defragmented last to avoid multiple etcd re-elections
   - defragmentation of one instance is finished before starting another
   - if there is any error for any instance, stop
   - the job is instantiated from cronjob/etcd-defrag-host

To determine the safety and eligible etcd instances, the job's pod talks to
prometheus instance which is expected to live in monitoring namespace and
scrapes etcd. Thus metrics like `etcd_server_leader_changes_seen_total` must be
available there.

## Alternative solutions

- [ugur99/etcd-defrag-cronjob](https://github.com/ugur99/etcd-defrag-cronjob)
   - ✓ does more-or-less the same in a single cronjob
   - ✗ expects connectivity to all etcd instances from pod network
   - ✗ cannot be used unless etcd instances are exposed somehow
   - ✗ no health checks
   - ✗ no activity for 1+years
   - ✗ uses etcdctl binary hardcoded in the repo
- [ahrtr/etcd-defrag](https://github.com/ahrtr/etcd-defrag/)
   - ✓ does more-or-less the same in a single cronjob
   - ✗ can be run either on the full cluster or against a single etcd instance
      - ✗ the "full cluster" mode again expects reachability of all instances
        from a pod or from a single node
      - ✓ single etcd instance mode would be usable when run on the host network
        with one cronjob for each master node, each with its own schedule
        different from the others
         - ✗ however, this would require hardcoding master node names for each k8s
           cluster we run in the values file
         - ✗ also, the schedule would have to be handcrafted manually for each
           cluster/node; moreover different clusters have different number of
           control plane nodes
   - ✓ actively maintained
   - ✓ health checks
- daemonset running pods on control plane nodes
   - ✓ simplest to implement
   - ✗ would require some kind of synchronization to avoid defragmentation of
     multiple instances at the same time
   - ✗ pods would be mostly idle, occupying resources and local storage
   - ✗ a failure of defragmentation would be left unnoticed unless some kind of
     custom alerting mechanism were built around it
   - ✗ overall cronjob feels like a much better fit for this use case
