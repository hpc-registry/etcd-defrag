CHART_NAME   ?= etcd-defrag
NAMESPACE    ?= kube-system
HELM_ARGS    += "$(CHART_NAME)" --namespace="$(NAMESPACE)" . -f values.yaml
REGISTRY     ?= registry.ethz.ch/hpc-registry/etcd-defrag

.PHONY: render
render:
	helm template $(HELM_ARGS)

.PHONY: lint
lint:
	make render --silent \
		| awk -v o=/dev/stderr '/^(apiVersion:|---)/ { o="/dev/stdout" } { print >o }' \
		| kube-linter lint --fail-if-no-objects-found -

run:
	make render --silent | kubectl create job --from=- etcd-defrag-manual-"$$(date '+%m-%d-%H-%M-%S')"

.PHONY: install
install:
	helm install --create-namespace $(HELM_ARGS)

.PHONY: upgrade
upgrade:
	helm upgrade $(HELM_ARGS)

# requires helm diff plugin
.PHONY: diff
diff:
	helm diff upgrade $(HELM_ARGS)

build:
	docker build -t $(REGISTRY) ./image/
